public class Application
{
	public static void main (String[] args)
	{
		Student stud1 = new Student();
		
		stud1.studentId = "123456";
		System.out.println("The first student's student ID: " + stud1.studentId);
		stud1.lockerNum = "1234";
		System.out.println("The first student's locker number: " + stud1.lockerNum);
		stud1.name = "Tyler";
		System.out.println("The first student's name: " + stud1.name);
		Student pres = new Student();
		
		pres.presentation(stud1.name);
		
		Student stud2 = new Student();
		
		stud2.studentId = "987654";
		System.out.println("The second student's student ID: " + stud2.studentId);
		stud2.lockerNum = "9876";
		System.out.println("The second student's locker number: " + stud2.lockerNum);
		stud2.name = "Hailey";
		System.out.println("The second student's name: " + stud2.name);
		
		pres.presentation(stud2.name);
		
		Student[] section3 = new Student[3];
		section3[0] = new Student();
		section3[0] = stud1;
		
		section3[1] = new Student();
		section3[1] = stud2;
		
		section3[2] = new Student();
		section3[2].studentId = "135790";
		section3[2].lockerNum = "1357";
		section3[2].name = "Bella";
		System.out.println(section3[0].studentId);
		System.out.println(section3[2].studentId);
		System.out.println(section3[2].lockerNum);
		System.out.println(section3[2].name);
	}
}